﻿using System;
using Volo.Abp.ObjectExtending;

namespace Volo.Abp.IdentityServer.ApiResources.Dtos
{
	public class CreateApiResourceDto : ExtensibleObject
	{
		public string Name { get; set; }

		public string DisplayName { get; set; }

		public string Description { get; set; }

		public string[] Claims { get; set; }
	}
}
