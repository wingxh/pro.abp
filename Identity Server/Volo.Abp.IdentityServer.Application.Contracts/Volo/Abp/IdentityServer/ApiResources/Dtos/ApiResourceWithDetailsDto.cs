﻿using System;
using System.Collections.Generic;
using Volo.Abp.Application.Dtos;

namespace Volo.Abp.IdentityServer.ApiResources.Dtos
{
	public class ApiResourceWithDetailsDto : ExtensibleEntityDto<Guid>
	{
		public string Name { get; set; }

		public string DisplayName { get; set; }

		public string Description { get; set; }

		public bool Enabled { get; set; }

		public List<ApiResourceClaimClaimDto> UserClaims { get; set; }

		public Dictionary<string, string> Properties { get; set; }

		public List<ApiScopeDto> Scopes { get; set; }

		public List<ApiSecretDto> Secrets { get; set; }
	}
}
