﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Volo.Abp.IdentityServer.ApiResources.Dtos
{
	public class ApiScopeDto
	{
		public Guid ApiResourceId { get; set; }

		[DisplayName("Name")]
		[Required]
		public string Name { get; set; }

		[DisplayName("DisplayName")]
		public string DisplayName { get; set; }

		[DisplayName("Description")]
		public string Description { get; set; }

		[DisplayName("Required")]
		public bool Required { get; set; }

		[DisplayName("Emphasize")]
		public bool Emphasize { get; set; }

		[DisplayName("ShowInDiscoveryDocument")]
		public bool ShowInDiscoveryDocument { get; set; }

		public ApiScopeClaimDto[] UserClaims { get; set; }
	}
}
