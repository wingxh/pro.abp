﻿using AutoMapper;
using Volo.Payment.Requests;

namespace Volo.Payment
{
    public class PaymentApplicationAutoMapperProfile : Profile
	{
		public PaymentApplicationAutoMapperProfile()
		{
			base.CreateMap<PaymentRequestProduct, PaymentRequestProductDto>();
			base.CreateMap<PaymentRequest, PaymentRequestWithDetailsDto>();
		}
	}
}
