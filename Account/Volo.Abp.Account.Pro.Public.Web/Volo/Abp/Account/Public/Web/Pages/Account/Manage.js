﻿(function ($) {

    var l = abp.localization.getResource('AbpAccount');

    var _profileService = volo.abp.identity.profile;

    var _confirmPhoneNumberModal = new abp.ModalManager(abp.appPath + 'Account/ConfirmPhoneNumberModal');

    $("#ChangePasswordForm").submit(function (e) {
        e.preventDefault();

        if (!$("#ChangePasswordForm").valid()) {
            return false;
        }

        var input = $("#ChangePasswordForm").serializeFormToObject().changePasswordInfoModel;

        if (input.newPassword != input.newPasswordConfirm || input.currentPassword == '') {
            abp.message.error(l("NewPasswordConfirmFailed"));
            return;
        }

        if (input.currentPassword == '') {
            return;
        }

        _profileService.changePassword(
            input
        ).then(function (result) {
            abp.message.success(l("PasswordChanged"));
            $("#ChangePasswordForm input").val('');
        });

    });

    $('#VerifyPhoneButton').on('click', '', function() {
        _confirmPhoneNumberModal.open();
    });

    $("#PersonalSettingsForm").submit(function (e) {
        e.preventDefault();

        if (!$("#PersonalSettingsForm").valid()) {
            return false;
        }

        var input = $("#PersonalSettingsForm").serializeFormToObject().personalSettingsInfoModel;

        _profileService.update(
            input
        ).then(function (result) {
            abp.notify.success(l("PersonalSettingsSaved"));

            if (!input.phoneNumber || input.phoneNumber === '') {
                $('#VerifyPhoneButton').hide();
                $('#PhoneVerified').hide();
                return;
            }

            if ($('#InitialPhoneNumber').val() === input.phoneNumber || $('#VerifyPhoneButton').length < 1) {
                return;
            }

            $('#VerifyPhoneButton').show();
            $('#PhoneVerified').hide();
            $('#InitialPhoneNumber').val(input.phoneNumber);

            abp.message.confirm(' ', l('DoYouWantToVerifyPhoneNumberMessage'),
                function(isConfirmed) {
                    if (isConfirmed) {
                        $('#VerifyPhoneButton').click();
                    }
                });
        });

    });

    _confirmPhoneNumberModal.onResult(function () {
        $('#VerifyPhoneButton').hide();
        $('#PhoneVerified').show();
    });


})(jQuery);
