﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.Identity;
using Volo.Abp.Settings;
using Volo.Abp.Users;

namespace Volo.Abp.Account.Public.Web.Pages.Account
{
	public class ManageModel : AccountPageModel
	{
		public ChangePasswordInfoModel ChangePasswordInfoModel { get; set; }

		public PersonalSettingsInfoModel PersonalSettingsInfoModel { get; set; }

		public Guid CurrentUserId { get; set; }

		public bool PhoneConfirmationEnabled { get; set; }

		private readonly IProfileAppService _profileAppService;

		public ManageModel(IProfileAppService profileAppService)
		{
			this._profileAppService = profileAppService;
		}

		public virtual async Task OnGetAsync()
		{
			var profileDto = await this._profileAppService.GetAsync();
			this.PersonalSettingsInfoModel = base.ObjectMapper.Map<ProfileDto, PersonalSettingsInfoModel>(profileDto);
			this.CurrentUserId = base.CurrentUser.GetId();
			var flag = await base.SettingProvider.IsTrueAsync("Abp.Identity.SignIn.EnablePhoneNumberConfirmation");
			this.PhoneConfirmationEnabled = flag;
		}

		public virtual Task<IActionResult> OnPostAsync()
		{
			return Task.FromResult<IActionResult>(this.Page());
		}
	}
}
