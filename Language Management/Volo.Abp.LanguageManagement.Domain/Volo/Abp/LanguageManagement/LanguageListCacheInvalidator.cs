﻿using System.Threading.Tasks;
using Volo.Abp.Caching;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Entities.Events;
using Volo.Abp.EventBus;

namespace Volo.Abp.LanguageManagement
{
    public class LanguageListCacheInvalidator : ILocalEventHandler<EntityChangedEventData<Language>>, ITransientDependency, IEventHandler
	{
		protected IDistributedCache<LanguageListCacheItem> Cache { get; }

		public LanguageListCacheInvalidator(IDistributedCache<LanguageListCacheItem> cache)
		{
			this.Cache = cache;
		}

		public virtual async Task HandleEventAsync(EntityChangedEventData<Language> eventData)
		{
			await this.Cache.RemoveAsync("AllLanguages");
		}
	}
}
